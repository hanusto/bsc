/*
 * (C) Copyright BSC, unpublished work, created 2015.
 * This computer program includes confidential, proprietary information
 * and is a trade secret of BSC. All use, disclosure, or reproduction
 * is prohibited unless authorized in writing by an officer of BSC.
 * All Rights Reserved.
 */

package com.bsc.tracker.ui.cli;

import org.apache.commons.cli.Option;

/**
 * Enumeration of options for CLI console.
 *
 * @author <a href="mailto:tomas.hanus@cleverlance.com">Tomas Hanus</a>
 */
public enum OptionEnum {

    HELP("h", "help", false, "Display help message"),
    ADD("a", "add", false, "Add new payment"),
    FILE("f", "file", true, "Import payments from file"),
    RATE("r", "rate", false, "Add new exchange rate"),
    LIST("l", "list", false, "List recorded payments"),
    QUIT("q", "quit", false, "Exit application");

    private final String opt;
    private final String longOpt;
    private final boolean hasArg;
    private final String description;

    private OptionEnum(String opt, String longOpt, boolean hasArg, String description) {
        this.opt = opt;
        this.longOpt = longOpt;
        this.hasArg = hasArg;
        this.description = description;
    }

    public String getOpt() {
        return opt;
    }

    public Option toOption() {
        return new Option(opt, longOpt, hasArg, description);
    }
}
