/*
 * (C) Copyright BSC, unpublished work, created 2015.
 * This computer program includes confidential, proprietary information
 * and is a trade secret of BSC. All use, disclosure, or reproduction
 * is prohibited unless authorized in writing by an officer of BSC.
 * All Rights Reserved.
 */

package com.bsc.tracker.ui.cli;

import java.io.Console;
import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.regex.Matcher;

import com.bsc.tracker.Config;
import com.bsc.tracker.bo.Payment;
import com.bsc.tracker.commons.PaymentLoader;
import com.bsc.tracker.commons.PaymentReporter;
import com.bsc.tracker.commons.converter.PaymentConverter;
import com.bsc.tracker.commons.converter.PaymentConverterImpl;
import com.bsc.tracker.service.PaymentService;
import com.bsc.tracker.service.PaymentServiceImpl;
import org.apache.commons.cli.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

/**
 * Console to interact with user.
 *
 * @author <a href="mailto:hanusto@gmail.com">Tomas Hanus</a>
 */
public class CliConsole {

    private final CommandLineParser parser;
    private final PaymentService service = new PaymentServiceImpl();
    private final PaymentConverter paymentConverter = new PaymentConverterImpl();

    private Options options;

    public CliConsole() {
        parser = new PosixParser();
        initOptions();
    }

    /**
     * Starts console handler to interaction with user.
     *
     * @param args input parameters
     */
    public void start(String[] args) {

        Console c = System.console();
        if (c == null) {
            System.err.println("General error: no console enabled");
            System.exit(1);
        }

        try {
            CommandLine cmd = parser.parse(options, args);
            if (cmd.hasOption(OptionEnum.FILE.getOpt())) {
                loadFromFile(cmd.getOptionValue(OptionEnum.FILE.getOpt()));
            }
        } catch (ParseException e) {
            System.err.println("Wrong input argument");
        } catch (Exception e) {
            System.err.println("Error occurred during initialization: " + e.getMessage());
        }

        showHelp();
        scheduleReporter();

        String[] commands;
        // infinite loop
        while (true) {
            // to encapsulate universal argument
            System.out.println("\n");
            commands = ("-" + c.readLine("Enter your command: ")).split(" ");
            CommandLine cmd;
            try {
                cmd = parser.parse(options, commands);

                if (cmd.hasOption(OptionEnum.LIST.getOpt())) {
                    showPayments();
                }

                if (cmd.hasOption(OptionEnum.ADD.getOpt())) {
                    commands = c.readLine("Add new payment (format 'XXX Y' where XXX is valid country code and Y is "
                        + "amount): ").split(" ");
                    addPayment(commands);
                }

                if (cmd.hasOption(OptionEnum.RATE.getOpt())) {
                    commands = c.readLine("Add new exchange rate: (format 'XXX YYY Z' where XXX and YYY is valid "
                        + "country code and Z is exchange multiplier): ").split(" ");
                    addExchangeRate(commands);
                }

                if (cmd.hasOption(OptionEnum.FILE.getOpt())) {
                    loadFromFile(cmd.getOptionValue(OptionEnum.FILE.getOpt()));
                }

                if (cmd.hasOption(OptionEnum.HELP.getOpt())) {
                    showHelp();
                }

                if (cmd.hasOption(OptionEnum.QUIT.getOpt())) {
                    System.exit(0);
                }
            } catch (ParseException e) {
                System.err.println("Wrong command");
                showHelp();
            }
        }
    }

    /**
     * Adds new payment into registry by user input.
     *
     * @param args the user input
     */
    void addPayment(String[] args) {
        try {
            Payment payment = parsePayment(args);
            if (payment != null) {
                service.insert(payment);
                System.out.println("New payment added.");
            }
        } catch (Exception e) {
            System.err.println("Error occurred: " + e.getMessage());
        }
    }

    /**
     * Parses user input and transforms it into {@link Payment}.
     *
     * @param args the user input
     * @return the {@link Payment}
     * @throws IllegalArgumentException occurs if user input is incorrect
     */
    Payment parsePayment(String[] args) throws IllegalArgumentException {
        try {
            Validate.notEmpty(args, "no parameters");
            Matcher matcher = PaymentLoader.PATTERN.matcher(StringUtils.join(args, " "));
            if (matcher.matches()) {
                return new Payment(matcher.group(1), new BigDecimal(matcher.group(2)));
            } else {
                throw new IllegalArgumentException("bad format of input");
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("wrong format of input parameters for command 'add': " + e.getMessage());
        }
    }

    /**
     * Adds new exchange rate.
     *
     * @param args the input arguments
     */
    void addExchangeRate(String[] args) {
        try {
            Validate.notEmpty(args, "no parameters");
            Validate.isTrue(args.length == 3, "bad format of input");
            paymentConverter.addExchangeRate(args[0], args[1], new BigDecimal(args[2]));
            System.out.println("New exchange rate added.");
        } catch (Exception e) {
            System.err.println("Error occurred: " + e.getMessage());
        }
    }

    /**
     * Loads payments from file.
     *
     * @param filePath the file path name
     */
    void loadFromFile(String filePath) {
        try {
            final List<Payment> paymentList = PaymentLoader.load(filePath);
            for (Payment payment : paymentList) {
                service.insert(payment);
            }
            System.out.println("Payments imported from file");
        } catch (Exception e) {
            System.err.println("Error occurred during loading file: " + e.getMessage());
        }
    }

    /**
     * Initialization of console options and arguments.
     */
    void initOptions() {
        options = new Options();
        options.addOption(OptionEnum.HELP.toOption());
        options.addOption(OptionEnum.FILE.toOption());
        options.addOption(OptionEnum.LIST.toOption());
        options.addOption(OptionEnum.ADD.toOption());
        options.addOption(OptionEnum.RATE.toOption());
        options.addOption(OptionEnum.QUIT.toOption());
    }

    /**
     * Action to show user help.
     */
    void showHelp() {
        System.out.println("\n");
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("Payment Tracker", "\n", options, "\n", true);
        System.out.println("\n");
    }

    /**
     * Action to show payments in registry.
     */
    void showPayments() {
        final List<Payment> result = service.list();
        System.out.println("\nPayment list, count: " + result.size());
        for (Payment payment : result) {
            System.out.print(payment.toHumanString());
            if (paymentConverter.isSupported(payment.getCurrency(), Config.DEFAULT_CURRENCY)) {
                System.out.print(" ("
                    + paymentConverter.convert(payment, Config.DEFAULT_CURRENCY).toHumanString() + ")");
            }
            System.out.print("\n");
        }
    }

    /**
     * Action to Schedule payment reporter that report list of payments in registry.
     */
    void scheduleReporter() {
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        final PaymentReporter paymentReporter = new PaymentReporter(service, paymentConverter, Config.DEFAULT_CURRENCY);
        scheduler.scheduleAtFixedRate(paymentReporter, Config.REPORT_DELAY, Config.REPORT_DELAY,
            Config.REPORT_DELAY_UNIT);
    }

    public PaymentService getService() {
        return service;
    }

    public PaymentConverter getPaymentConverter() {
        return paymentConverter;
    }
}
