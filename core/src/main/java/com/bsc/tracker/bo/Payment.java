/*
 * (C) Copyright BSC, unpublished work, created 2015.
 * This computer program includes confidential, proprietary information
 * and is a trade secret of BSC. All use, disclosure, or reproduction
 * is prohibited unless authorized in writing by an officer of BSC.
 * All Rights Reserved.
 */

package com.bsc.tracker.bo;

import java.math.BigDecimal;

import com.bsc.tracker.commons.HumanReadable;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.joda.time.DateTime;

/**
 * The holder class that represents single payment.
 *
 * @author <a href="mailto:hanusto@gmail.com">Tomas Hanus</a>
 */
public class Payment implements HumanReadable {

    private final String currency;
    private final BigDecimal amount;
    private final DateTime transactionTs;

    /**
     * Default constructor to create new payment object.
     *
     * @param currency the currency with appropriate code
     * @param amount   the amount
     * @throws IllegalArgumentException occurs if currency or amount is not specified correctly
     */
    public Payment(String currency, BigDecimal amount, DateTime transactionTs) throws IllegalArgumentException {
        Validate.notNull(currency, "the currency must not be null");
        Validate.notNull(amount, "the amount must not be null");
        Validate.notNull(transactionTs, "the transactionTs must not be null");
        this.currency = currency;
        this.amount = amount;
        this.transactionTs = transactionTs;
    }

    /**
     * Default constructor to create new payment object.
     *
     * @param currency the currency with appropriate code
     * @param amount   the amount
     * @throws IllegalArgumentException occurs if currency or amount is not specified correctly
     */
    public Payment(String currency, BigDecimal amount) throws IllegalArgumentException {
        this(currency, amount, DateTime.now());
    }

    public String getCurrency() {
        return currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public DateTime getTransactionTs() {
        return transactionTs;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) { return false; }
        if (obj == this) { return true; }
        if (obj.getClass() != getClass()) {
            return false;
        }
        Payment rhs = (Payment) obj;
        return new EqualsBuilder()
            .append(this.currency, rhs.currency)
            .append(this.amount, rhs.amount)
            .append(this.transactionTs, rhs.transactionTs)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
            .append(currency)
            .append(amount)
            .append(transactionTs)
            .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
            .append("currency", currency)
            .append("amount", amount)
            .append("transactionTs", transactionTs)
            .toString();
    }

    @Override
    public String toHumanString() {
        return getCurrency() + " " + getAmount();
    }
}
