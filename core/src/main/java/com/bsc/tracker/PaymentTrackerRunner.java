/*
 * (C) Copyright BSC, unpublished work, created 2015.
 * This computer program includes confidential, proprietary information
 * and is a trade secret of BSC. All use, disclosure, or reproduction
 * is prohibited unless authorized in writing by an officer of BSC.
 * All Rights Reserved.
 */

package com.bsc.tracker;

import com.bsc.tracker.ui.cli.CliConsole;

/**
 * Initialization runner class for application.
 *
 * @author <a href="mailto:hanusto@gmail.com">Tomas Hanus</a>
 */
public class PaymentTrackerRunner {

    public static void main(String[] args) {
        new CliConsole().start(args);
    }
}