/*
 * (C) Copyright BSC, unpublished work, created 2015.
 * This computer program includes confidential, proprietary information
 * and is a trade secret of BSC. All use, disclosure, or reproduction
 * is prohibited unless authorized in writing by an officer of BSC.
 * All Rights Reserved.
 */

package com.bsc.tracker.commons.converter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.bsc.tracker.bo.Payment;
import com.bsc.tracker.commons.CurrencyValidator;
import org.apache.commons.lang3.Validate;

/**
 * Converter to exchange amount among supported currency.
 *
 * @author <a href="mailto:hanusto@gmail.com">Tomas Hanus</a>
 */
public class PaymentConverterImpl implements PaymentConverter {

    private final Map<ExchangeRate, BigDecimal> exchangeRates;

    public PaymentConverterImpl() {
        this.exchangeRates = new ConcurrentHashMap<>();
    }

    @Override
    public Payment convert(Payment payment, String currency) {
        Validate.notNull(payment, "the payment must not be null");
        Validate.notEmpty(currency, "the currency must not be empty");

        BigDecimal multiplier = exchangeRates.get(new ExchangeRate(payment.getCurrency(), currency));
        if (multiplier == null) {
            multiplier = exchangeRates.get(new ExchangeRate(currency, payment.getCurrency()));
            if (multiplier == null) {
                throw new IllegalArgumentException("Payment [" + payment.toHumanString() + " cannot be converted to '"
                    + currency + "' because this exchange is not registered");
            }
            return new Payment(currency, payment.getAmount().divide(multiplier, 1, RoundingMode.HALF_UP));
        } else {
            return new Payment(currency, payment.getAmount().multiply(multiplier));
        }
    }

    @Override
    public void addExchangeRate(String currencyFrom, String currencyTo, BigDecimal multiplier)
        throws IllegalArgumentException {
        CurrencyValidator.validate(currencyFrom);
        CurrencyValidator.validate(currencyTo);
        Validate.notNull(multiplier, "the multiplier must not be null");
        Validate.isTrue(multiplier.compareTo(new BigDecimal("0")) != 0, "the multiplier must not be zero");
        exchangeRates.put(new ExchangeRate(currencyFrom, currencyTo), multiplier);
    }

    @Override
    public boolean isSupported(String currencyFrom, String currencyTo) {
        return getMultiplier(currencyFrom, currencyTo) != null;
    }

    BigDecimal getMultiplier(String currencyFrom, String currencyTo) {
        BigDecimal multiplier = exchangeRates.get(new ExchangeRate(currencyFrom, currencyTo));
        if (multiplier == null) {
            multiplier = exchangeRates.get(new ExchangeRate(currencyTo, currencyFrom));
        }
        return multiplier;
    }
}
