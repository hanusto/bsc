/*
 * (C) Copyright BSC, unpublished work, created 2015.
 * This computer program includes confidential, proprietary information
 * and is a trade secret of BSC. All use, disclosure, or reproduction
 * is prohibited unless authorized in writing by an officer of BSC.
 * All Rights Reserved.
 */

package com.bsc.tracker.commons;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.bsc.tracker.bo.Payment;

/**
 * Loader of payments from file.
 *
 * @author <a href="mailto:hanusto@gmail.com">Tomas Hanus</a>
 */
public class PaymentLoader {

    public static final Pattern PATTERN = Pattern.compile("^([A-Z]{3}) (-?\\d+(\\.\\d{2})?)$");

    /**
     * Load payments from file.
     *
     * @param file the file path name
     * @return the loaded payment records
     * @throws IllegalArgumentException occurs if file does not exist or has wrong format
     */
    public static List<Payment> load(File file) throws IllegalArgumentException {
        List<Payment> result = new LinkedList<>();

        try {
            Scanner in = new Scanner(new FileReader(file));
            while (in.hasNextLine()) {
                Matcher matcher = PATTERN.matcher(in.nextLine());
                if (matcher.matches()) {
                    result.add(new Payment(matcher.group(1), new BigDecimal(matcher.group(2))));
                } else {
                    throw new IllegalArgumentException("bad format of input");
                }
            }
            return result;
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("file '" + file + "' does not exist");
        }
    }

    /**
     * Load payments from file.
     *
     * @param file the file path name
     * @return the loaded payment records
     * @throws IllegalArgumentException occurs if file does not exist or has wrong format
     */
    public static List<Payment> load(String file) throws IllegalArgumentException {
        return load(new File(file));
    }
}
