/*
 * (C) Copyright BSC, unpublished work, created 2015.
 * This computer program includes confidential, proprietary information
 * and is a trade secret of BSC. All use, disclosure, or reproduction
 * is prohibited unless authorized in writing by an officer of BSC.
 * All Rights Reserved.
 */

package com.bsc.tracker.commons;

/**
 * Contract for human readable entity.
 *
 * @author <a href="mailto:hanusto@gmail.com">Tomas Hanus</a>
 */
public interface HumanReadable {

    /**
     * Returns human readable entity identification with start and end round brackets.
     *
     * @return entity identification
     */
    String toHumanString();

}
