/*
 * (C) Copyright BSC, unpublished work, created 2015.
 * This computer program includes confidential, proprietary information
 * and is a trade secret of BSC. All use, disclosure, or reproduction
 * is prohibited unless authorized in writing by an officer of BSC.
 * All Rights Reserved.
 */

package com.bsc.tracker.commons;

import java.util.List;

import com.bsc.tracker.bo.Payment;
import com.bsc.tracker.commons.converter.PaymentConverter;
import com.bsc.tracker.service.PaymentService;

/**
 * Reporter for payment records.
 *
 * @author <a href="mailto:hanusto@gmail.com">Tomas Hanus</a>
 */
public class PaymentReporter implements Runnable {

    private final PaymentService service;
    private final PaymentConverter converter;
    private final String defaultCurrency;

    public PaymentReporter(PaymentService service, PaymentConverter converter, String defaultCurrency) {
        this.service = service;
        this.converter = converter;
        this.defaultCurrency = defaultCurrency;
    }

    @Override
    public void run() {
        final List<Payment> result = service.list();
        System.out.println("\nPayment list, count: " + result.size());
        for (Payment payment : result) {
            System.out.print(payment.toHumanString());
            if (converter.isSupported(payment.getCurrency(), defaultCurrency)) {
                System.out.print(" (" + converter.convert(payment, defaultCurrency).toHumanString() + ")");
            }
            System.out.print("\n");
        }
        System.out.println("\n");
    }
}
