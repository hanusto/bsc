/*
 * (C) Copyright BSC, unpublished work, created 2015.
 * This computer program includes confidential, proprietary information
 * and is a trade secret of BSC. All use, disclosure, or reproduction
 * is prohibited unless authorized in writing by an officer of BSC.
 * All Rights Reserved.
 */

package com.bsc.tracker.commons.converter;

import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Holder class for exchange rate between currencies.
 *
 * @author <a href="mailto:hanusto@gmail.com">Tomas Hanus</a>
 */
class ExchangeRate {

    private final String currencyFrom;
    private final String currencyTo;

    ExchangeRate(String currencyFrom, String currencyTo) {
        Validate.notEmpty(currencyFrom, "the currencyFrom must not be empty");
        Validate.notEmpty(currencyTo, "the currencyTo must not be empty");
        this.currencyFrom = currencyFrom;
        this.currencyTo = currencyTo;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) { return false; }
        if (obj == this) { return true; }
        if (obj.getClass() != getClass()) {
            return false;
        }
        ExchangeRate rhs = (ExchangeRate) obj;
        return new EqualsBuilder()
            .append(this.currencyFrom, rhs.currencyFrom)
            .append(this.currencyTo, rhs.currencyTo)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
            .append(currencyFrom)
            .append(currencyTo)
            .toHashCode();
    }
}