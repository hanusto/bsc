/*
 * (C) Copyright BSC, unpublished work, created 2015.
 * This computer program includes confidential, proprietary information
 * and is a trade secret of BSC. All use, disclosure, or reproduction
 * is prohibited unless authorized in writing by an officer of BSC.
 * All Rights Reserved.
 */

package com.bsc.tracker.commons;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.Validate;

/**
 * Validator for correctly short code of currency.
 *
 * @author <a href="mailto:hanusto@gmail.com">Tomas Hanus</a>
 */
public class CurrencyValidator {

    private static final Pattern PATTERN = Pattern.compile("^([A-Z]{3})$");

    /**
     * Validates currency.
     *
     * @param currency the input currency
     * @throws IllegalArgumentException occurs if input parameter has wrong format of currency
     * @see #PATTERN
     */
    public static void validate(String currency) throws IllegalArgumentException {
        Validate.notEmpty(currency, "the currency must not be empty");
        Matcher matcher = PATTERN.matcher(currency);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("bad format of currency: " + currency);
        }
    }
}
