/*
 * (C) Copyright BSC, unpublished work, created 2015.
 * This computer program includes confidential, proprietary information
 * and is a trade secret of BSC. All use, disclosure, or reproduction
 * is prohibited unless authorized in writing by an officer of BSC.
 * All Rights Reserved.
 */

package com.bsc.tracker.commons.converter;

import java.math.BigDecimal;

import com.bsc.tracker.bo.Payment;

/**
 * Contract to payment converter to transform amount among currencies.
 *
 * @author <a href="mailto:tomas.hanus@cleverlance.com">Tomas Hanus</a>
 */
public interface PaymentConverter {

    /**
     * Converts {@link Payment} to equivalent amount for specific currency.
     *
     * @param payment  the payment
     * @param currency the currency
     * @return the converted payment into specific currency
     */
    Payment convert(Payment payment, String currency);

    /**
     * Checks whether specific exchange is supported or not.
     *
     * @param currencyFrom the currency from
     * @param currencyTo   the currency to
     * @return true if this exchange is supported otherwise false
     */
    boolean isSupported(String currencyFrom, String currencyTo);

    /**
     * Adds new exchange rate to registry.
     *
     * @param currencyFrom the currency from
     * @param currencyTo   the currency to
     * @param multiplier   the multiplier
     * @throws IllegalArgumentException
     */
    void addExchangeRate(String currencyFrom, String currencyTo, BigDecimal multiplier);
}
