/*
 * (C) Copyright BSC, unpublished work, created 2015.
 * This computer program includes confidential, proprietary information
 * and is a trade secret of BSC. All use, disclosure, or reproduction
 * is prohibited unless authorized in writing by an officer of BSC.
 * All Rights Reserved.
 */

package com.bsc.tracker.service;

import java.util.List;

import com.bsc.tracker.bo.Payment;

/**
 * Contract service to manipulate with payments.
 *
 * @author <a href="mailto:hanusto@gmail.com">Tomas Hanus</a>
 */
public interface PaymentService {

    /**
     * Inserts payment into repository.
     *
     * @param payment the payment
     * @return the new inserted payment
     */
    Payment insert(Payment payment);

    /**
     * Finds all payments in repository.
     *
     * @return all payments
     */
    List<Payment> findAll();

    /**
     * List all payments from repository.
     */
    List<Payment> list();
}
