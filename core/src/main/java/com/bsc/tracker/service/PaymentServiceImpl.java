/*
 * (C) Copyright BSC, unpublished work, created 2015.
 * This computer program includes confidential, proprietary information
 * and is a trade secret of BSC. All use, disclosure, or reproduction
 * is prohibited unless authorized in writing by an officer of BSC.
 * All Rights Reserved.
 */

package com.bsc.tracker.service;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.bsc.tracker.bo.Payment;
import com.bsc.tracker.dao.PaymentDao;
import com.bsc.tracker.dao.PaymentDaoMemoryImpl;

/**
 * Contract default implementation of {@link PaymentService}.
 *
 * @author <a href="mailto:hanusto@gmail.com">Tomas Hanus</a>
 */
public class PaymentServiceImpl implements PaymentService {

    private final PaymentDao dao = new PaymentDaoMemoryImpl();

    private static final BigDecimal ZERO_AMOUNT = new BigDecimal("0");

    @Override
    public Payment insert(Payment payment) {
        return dao.insert(payment);
    }

    @Override
    public List<Payment> findAll() {
        return Collections.unmodifiableList(dao.findAll());
    }

    @Override
    public List<Payment> list() {
        List<Payment> result = new LinkedList<>();
        for (Payment payment : findAll()){
            if (payment.getAmount().compareTo(ZERO_AMOUNT) != 0) {
                result.add(payment);
            }
        }
        return Collections.unmodifiableList(result);
    }
}
