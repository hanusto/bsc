/*
 * (C) Copyright BSC, unpublished work, created 2015.
 * This computer program includes confidential, proprietary information
 * and is a trade secret of BSC. All use, disclosure, or reproduction
 * is prohibited unless authorized in writing by an officer of BSC.
 * All Rights Reserved.
 */

package com.bsc.tracker;

import java.util.concurrent.TimeUnit;

/**
 * Configuration for application.
 *
 * @author <a href="mailto:hanusto@gmail.com">Tomas Hanus</a>
 */
public class Config {

    public static final String DEFAULT_CURRENCY = "USD";
    public static final int REPORT_DELAY = 1;
    public static final TimeUnit REPORT_DELAY_UNIT = TimeUnit.MINUTES;

}
