/*
 * (C) Copyright BSC, unpublished work, created 2015.
 * This computer program includes confidential, proprietary information
 * and is a trade secret of BSC. All use, disclosure, or reproduction
 * is prohibited unless authorized in writing by an officer of BSC.
 * All Rights Reserved.
 */

package com.bsc.tracker.dao;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.bsc.tracker.bo.Payment;

/**
 * Memory implementation of {@link PaymentDao} contract as default repository.
 *
 * @author <a href="mailto:hanusto@gmail.com">Tomas Hanus</a>
 */
public class PaymentDaoMemoryImpl implements PaymentDao {

    private final List<Payment> repository = new CopyOnWriteArrayList<>();

    @Override
    public Payment insert(Payment payment) {
        repository.add(payment);
        return payment;
    }

    @Override
    public List<Payment> findAll() {
        return Collections.unmodifiableList(repository);
    }
}
