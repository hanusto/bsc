/*
 * (C) Copyright BSC, unpublished work, created 2015.
 * This computer program includes confidential, proprietary information
 * and is a trade secret of BSC. All use, disclosure, or reproduction
 * is prohibited unless authorized in writing by an officer of BSC.
 * All Rights Reserved.
 */

package com.bsc.tracker.ui.cli;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

import java.math.BigDecimal;
import java.util.List;

import com.bsc.tracker.bo.Payment;
import org.testng.annotations.Test;

/**
 * Test suites for CLI console.
 *
 * @author <a href="mailto:hanusto@gmail.com">Tomas Hanus</a>
 */
public class CliConsoleTest {

    @Test
    public void testAddPayment() throws Exception {
        CliConsole console = new CliConsole();
        console.addPayment(new String[]{"USD", "111.50"});

        final List<Payment> paymentList = console.getService().list();
        assertEquals(paymentList.get(0).getCurrency(), "USD");
        assertEquals(paymentList.get(0).getAmount(), new BigDecimal("111.50"));
    }

    @Test
    public void testParsePayment() throws Exception {
        CliConsole console = new CliConsole();
        final Payment actual = console.parsePayment(new String[]{"USD", "111.50"});

        assertEquals(actual.getCurrency(), "USD");
        assertEquals(actual.getAmount(), new BigDecimal("111.50"));
    }

    @Test
    public void testAddPayment_wrongFormat() throws Exception {
        CliConsole console = new CliConsole();
        try {
            console.parsePayment(new String[]{"US", "111"});
            fail("Wrong format of input exception expected");
        } catch (Exception e) {
            assertEquals(e.getMessage(), "wrong format of input parameters for command 'add': bad format of input");
        }
    }

    @Test
    public void testAddExchangeRate() throws Exception {
        CliConsole console = new CliConsole();
        console.addExchangeRate(new String[]{"CZK", "USD", "111.50"});

        assertEquals(console.getPaymentConverter().isSupported("CZK", "USD"), true);
    }
}
