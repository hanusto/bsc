/*
 * (C) Copyright BSC, unpublished work, created 2015.
 * This computer program includes confidential, proprietary information
 * and is a trade secret of BSC. All use, disclosure, or reproduction
 * is prohibited unless authorized in writing by an officer of BSC.
 * All Rights Reserved.
 */

package com.bsc.tracker.bo;

import static org.testng.Assert.assertEquals;

import java.math.BigDecimal;

import org.joda.time.DateTime;
import org.testng.annotations.Test;

/**
 * Test suites for {@link Payment} object.
 *
 * @author <a href="mailto:hanusto@gmail.com">Tomas Hanus</a>
 */
public class PaymentTest {

    @Test
    public void testEquals() throws Exception {
        Payment p1 = new Payment("CZK", new BigDecimal("5"));

        assertEquals(p1.equals(p1), true);
        Payment p2 = new Payment("USD", new BigDecimal("5"));
        assertEquals(p1.equals(p2), false);
    }

    @Test
    public void testHashCode() throws Exception {
        Payment p1 = new Payment("CZK", new BigDecimal("5"), DateTime.parse("2015-03-14"));

        assertEquals(p1.hashCode(), 1093197641);
    }
}
