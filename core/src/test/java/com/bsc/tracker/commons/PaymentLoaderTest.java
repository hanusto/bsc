/*
 * (C) Copyright BSC, unpublished work, created 2015.
 * This computer program includes confidential, proprietary information
 * and is a trade secret of BSC. All use, disclosure, or reproduction
 * is prohibited unless authorized in writing by an officer of BSC.
 * All Rights Reserved.
 */

package com.bsc.tracker.commons;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

import java.util.List;

import com.bsc.tracker.bo.Payment;
import org.testng.annotations.Test;

/**
 * Test suites for loader of payments from file.
 *
 * @author <a href="mailto:hanusto@gmail.com">Tomas Hanus</a>
 */
public class PaymentLoaderTest {

    @Test
    public void testLoadFile() throws Exception {

        final List<Payment> paymentList = PaymentLoader.load(getClass().getResource("/payments.txt").getFile());
        assertEquals(paymentList.size(), 5);

    }

    @Test
    public void testLoadFile_fileDoesNotExit() throws Exception {

        try {
            PaymentLoader.load("/payments.txt");
            fail("file does not exist");
        } catch (Exception e) {
            assertEquals(e.getMessage(), "file '\\payments.txt' does not exist");
        }
    }

    @Test(expectedExceptions = IllegalArgumentException.class,
        expectedExceptionsMessageRegExp = "bad format of input")
    public void testLoadFile_wrongFormat() throws Exception {
        PaymentLoader.load(getClass().getResource("/payments_wrong.txt").getFile());
    }
}
