/*
 * (C) Copyright BSC, unpublished work, created 2015.
 * This computer program includes confidential, proprietary information
 * and is a trade secret of BSC. All use, disclosure, or reproduction
 * is prohibited unless authorized in writing by an officer of BSC.
 * All Rights Reserved.
 */

package com.bsc.tracker.commons.converter;

import static org.testng.Assert.assertEquals;

import java.math.BigDecimal;

import com.bsc.tracker.bo.Payment;
import org.testng.annotations.Test;

/**
 * Test suites for currency converter {@link PaymentConverterImpl}.
 *
 * @author <a href="mailto:hanusto@gmail.com">Tomas Hanus</a>
 */
public class PaymentConverterImplTest {

    @Test
    public void testConvert() throws Exception {
        PaymentConverter converter = new PaymentConverterImpl();
        converter.addExchangeRate("CZK", "USD", new BigDecimal("0.038447"));


        Payment payment = new Payment("CZK", new BigDecimal("1"));
        assertEquals(converter.convert(payment, "USD").getAmount(), new BigDecimal("0.038447"));

        payment = new Payment("USD", new BigDecimal("0.038447"));
        assertEquals(converter.convert(payment, "CZK").getAmount(), new BigDecimal("1.0"));

        // Non-terminating decimal expansion; no exact representable decimal result
        payment = new Payment("USD", new BigDecimal("500"));
        assertEquals(converter.convert(payment, "CZK").getAmount(), new BigDecimal("13004.9"));
    }

    @Test(expectedExceptions = IllegalArgumentException.class,
        expectedExceptionsMessageRegExp = "the multiplier must not be zero")
    public void testConvert_wrongMultiplier() throws Exception {
        PaymentConverter converter = new PaymentConverterImpl();
        converter.addExchangeRate("CZK", "USD", new BigDecimal("0"));
    }
}
