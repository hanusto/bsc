/*
 * (C) Copyright BSC, unpublished work, created 2015.
 * This computer program includes confidential, proprietary information
 * and is a trade secret of BSC. All use, disclosure, or reproduction
 * is prohibited unless authorized in writing by an officer of BSC.
 * All Rights Reserved.
 */

package com.bsc.tracker.commons;

import org.testng.annotations.Test;

/**
 * Test suites for currency validator, {@link CurrencyValidator}.
 *
 * @author <a href="mailto:hanusto@gmail.com">Tomas Hanus</a>
 */
public class CurrencyValidatorTest {

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testValidate() throws Exception {
        CurrencyValidator.validate("A");
    }
}
