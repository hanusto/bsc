/*
 * (C) Copyright BSC, unpublished work, created 2015.
 * This computer program includes confidential, proprietary information
 * and is a trade secret of BSC. All use, disclosure, or reproduction
 * is prohibited unless authorized in writing by an officer of BSC.
 * All Rights Reserved.
 */

package com.bsc.tracker.service;

import static org.testng.Assert.assertEquals;

import java.math.BigDecimal;

import com.bsc.tracker.bo.Payment;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * Test suites for service {@link PaymentServiceImpl}.
 *
 * @author <a href="mailto:hanusto@gmail.com">Tomas Hanus</a>
 */
public class PaymentServiceImplTest {

    private final PaymentService service = new PaymentServiceImpl();

    @BeforeTest
    public void init() {
        service.insert(new Payment("CZK", new BigDecimal("5")));
        service.insert(new Payment("USD", new BigDecimal("0")));
    }

    @Test
    public void testFindAll() throws Exception {
        assertEquals(service.findAll().size(), 2);
    }

    @Test
    public void testList() throws Exception {
        assertEquals(service.list().size(), 1);
    }
}
