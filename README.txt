About Payment Tracker:
=====================================
- keeps a record of payments. Each payment includes a currency and an amount.
- lists a list of all the currency and amounts to the console once per minute or manually by command 'list'.
- the input can be typed into the command line by command 'add'.
- payments can also be loaded from a file when starting up by parameter -file filename.ext.

Program uses external libs:
- joda-time-2.7.jar
- commons-lang3-3.1.jar
- bsh-2.0b4.jar
- jcommander-1.27.jar
- testng-6.8.21.jar
- commons-cli-1.2.jar
- jsr305-1.3.7.jar

All of them are available on public maven repository. After package phase these libs are copied into core/target/payment-tracker.lib sub-folder.

How to run:
=====================================
step1) in root package (where this file is located) execute: mvn package
step2) in root package (where this file is located) execute: java -jar core/target/payment-tracker.jar
    - optionally with parameter -file filename.ext to load payments from file

Help:
=====================================
- each command is performed by pressing ENTER key
- command can have parameters (command 'file') or custom wizard ('add' or 'rate' command)
- by command 'h' or 'help' you can show help message of Payment Tracker

usage: Payment Tracker [-a] [-f <arg>] [-h] [-l] [-q] [-r]
 add          Add new payment. Expected format on input is: 'XXX Y' where XXX is valid
              country code and Y is amount, e.g. 'USD 350'
 rate         Add new exchange rate. Expected format on input is: 'XXX YYY Z' where XXX
              and YYY is valid country code and Z is exchange
              multiplier, e.g. 'USD CZK 0.038'
 file <arg>   Import payments from file, e.g. 'file filename.ext'
 help         Display help message
 list         List recorded payments
 quit         Exit application

Note:
=====================================
If error occurs during parsing input, error message is shown but program is continuing.
